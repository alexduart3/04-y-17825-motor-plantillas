const express = require("express");
const router = express.Router();
const {getAll, run, getLastOrder, matriculaExistente, getLastId} = require('../db/conexion');
const fs = require('fs').promises;
const multer = require('multer');
const upload = multer({ dest: "./public/assets/images"});
const fileUpload = upload.single('src');

router.get("/", (req, res) => {
    res.render("admin/index");
});

// Integrantes
router.get("/integrantes/listar", async (req, res) => {
    const integrantes = await getAll("select * from integrantes where activo = 1 order by orden");
    res.render("admin/integrantes/index", {
        integrantes: integrantes
    });
});

router.get("/integrantes/crear", (req, res) => {
    res.render("admin/integrantes/crearIntegrante", {
        matricula: req.query.matricula,
        nombre: req.query.nombre,
        apellido: req.query.apellido
    });
});

router.post("/integrantes/create", async (req, res) => {

    let errores = [];

    const lastOrder = await getLastOrder('integrantes');
    const newOrder = lastOrder + 1;

    if (req.body.matricula === '') {
        errores.push('¡La matrícula no puede estar vacía!');
    }

    if (await matriculaExistente(req.body.matricula)) {
        errores.push('¡La matrícula ya existe!');
    }

    if (req.body.nombre === '' || req.body.nombre.length > 50) {
        errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
    }

    if (req.body.apellido === '' || req.body.apellido.length > 50) {
        errores.push('¡El apellido no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
    }

    if (req.body.activo === undefined) {
        errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
    }

    if (errores.length > 0) {
        res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent(errores.join(';'))}
        &matricula=${encodeURIComponent(req.body.matricula)}
        &nombre=${encodeURIComponent(req.body.nombre)}
        &apellido=${encodeURIComponent(req.body.apellido)}`);
    } else {
        try {
            await run("insert into integrantes (orden, matricula, nombre, apellido, activo) values (?, ?, ?, ?, ?)",
                [
                    newOrder,
                    req.body.matricula,
                    req.body.nombre,
                    req.body.apellido,
                    req.body.activo
                ]);
            res.redirect(`/admin/integrantes/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (err) {
            console.log(err);
            res.redirect(`/admin/integrantes/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    }
});

// Tipo Media
router.get("/tipoMedia/listar", async (req, res) => {
    const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by orden");
    res.render("admin/tipoMedia/index", {
        tipoMedia: tipoMedia
    });
});

router.get("/tipoMedia/crear", (req, res) => {
    res.render("admin/tipoMedia/crearTipoMedia", {
        nombre: req.query.nombre,
    });
});

router.post("/tipoMedia/create", async (req, res) => {

    let errores = [];

    const lastOrder = await getLastOrder('tipoMedia');
    const newOrder = lastOrder + 1;
    const lastId = await getLastId('tipoMedia');
    const newId = lastId + 1;

    if (req.body.nombre === '' || req.body.nombre.length > 50) {
        errores.push('¡El nombre no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
    }

    if (req.body.activo === undefined) {
        errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
    }

    if (errores.length > 0) {
        res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent(errores.join(';'))}&nombre=${encodeURIComponent(req.body.nombre)}`);
    } else {
        try {
            await run("insert into tipoMedia (id, orden, nombre, activo) values (?, ?, ?, ?)",
                [
                    newId,
                    newOrder,
                    req.body.nombre,
                    req.body.activo
                ]);
            res.redirect(`/admin/tipoMedia/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (err) {
            console.log(err);
            res.redirect(`/admin/tipoMedia/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    }
});

// Media
router.get("/media/listar", async (req, res) => {
    const media = await getAll(`
        SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
        FROM media
        LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
        LEFT JOIN integrantes ON media.matricula = integrantes.matricula
        WHERE media.activo = 1
        ORDER BY media.orden
    `);
    res.render("admin/media/index", {
        media: media,
    });
});

router.get("/media/crear", async (req, res) => {
    const integrantes = await getAll("select * from integrantes where activo = 1 order by nombre");
    const tipoMedia = await getAll("select * from tipoMedia where activo = 1 order by nombre");
    res.render("admin/media/crearMedia", {
        integrantes: integrantes,
        tipoMedia: tipoMedia,
        tipoMediaSeleccionado: req.query.tipoMedia,
        url: req.query.url,
        titulo: req.query.titulo,
        alt: req.query.alt,
        integranteSeleccionado: req.query.integrante,
    });
});

router.post("/media/create", fileUpload, async (req, res) => {

    let errores = [];

    const lastOrder = await getLastOrder('media');
    const newOrder = lastOrder + 1;
    const lastId = await getLastId('media');
    const newId = lastId + 1;

    if (req.body.tipoMedia === '' || req.body.tipoMedia === undefined) {
        errores.push('¡Debe seleccionar un tipo de media!');
    }

    if (req.body.integrante === '' || req.body.integrante === undefined) {
        errores.push('¡Debe seleccionar un integrante!');
    }

    if (req.body.url && req.file) {
        errores.push('¡No puedes agregar tanto URL como SRC al mismo tiempo!');
    }

    if (req.body.titulo === '' || req.body.titulo.length > 50) {
        errores.push('¡El título no puede estar vacío y debe tener una longitud máxima de 50 caracteres!');
    }

    if (req.body.alt === '' || req.body.titulo.length > 80) {
        errores.push('¡El alt no puede estar vacío y debe tener una longitud máxima de 80 caracteres!');
    }

    if (req.body.activo === undefined) {
        errores.push('¡Debe seleccionar un estado (activo o inactivo)!');
    }

    if (errores.length > 0) {
        res.redirect(`/admin/media/crear?error=${encodeURIComponent(errores.join(';'))}
            &tipoMedia=${encodeURIComponent(req.body.tipoMedia)}
            &url=${encodeURIComponent(req.body.url)}
            &titulo=${encodeURIComponent(req.body.titulo)}
            &alt=${encodeURIComponent(req.body.alt)}
            &integrante=${encodeURIComponent(req.body.integrante)}`);
    } else {
        let srcPath = '';
        if (req.file) {
            var tpm_path = req.file.path;
            var destino = "public/assets/images/" + req.file.originalname;
            try {
                await fs.rename(tpm_path, destino);
                srcPath = "/assets/images/" + req.file.originalname;
            } catch (err) {
                return res.sendStatus(500);
            }
        }
        try {
            await run("insert into media (id, url, src, titulo, alt, tipoMedia, matricula, orden, activo) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [
                    newId,
                    req.body.url,
                    srcPath,
                    req.body.titulo,
                    req.body.alt,
                    req.body.tipoMedia,
                    req.body.integrante,
                    newOrder,
                    req.body.activo,
                ]);
            res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
        } catch (err) {
            console.log(err);
            res.redirect(`/admin/media/crear?error=${encodeURIComponent('¡Error al insertar el registro!')}`);
        }
    }
});

module.exports = router;
