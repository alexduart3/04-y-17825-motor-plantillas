const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");
const {request, response} = require("express");
const hbs = require("hbs");
require("dotenv").config();

const app = express();
const router = require('./routes/public');
const routerAdmin = require('./routes/admin');

app.use(bodyParser.urlencoded({ extended: false }));

app.use("/", router);
app.use("/admin", routerAdmin);

// Servir archivos estáticos desde la carpeta "public"
app.use(express.static(__dirname + "/public"));
app.set("view engine", "hbs");
app.set("views", __dirname + "/views");

hbs.registerPartials(__dirname + "/views/partials");

// Helper para comparar valores y seleccionar la opción correcta en un select
hbs.registerHelper('eq', function(a, b) {
    return a === b;
});

app.use((req, res) => {
    res.status(404).render('page_404');
});

const puerto = process.env.PORT || 3001;

// Iniciar el servidor en el puerto 3001
app.listen(puerto, () => {
    console.log("Servidor corriendo en el puerto " + puerto);
});

// console.log('Base de Datos estática: ', bd);
